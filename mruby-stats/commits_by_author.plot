set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "Yukihiro Matsumoto" w lines, 'commits_by_author.dat' using 1:3 title "Yukihiro \"Matz\" Matsumoto" w lines, 'commits_by_author.dat' using 1:4 title "Daniel Bovensiepen" w lines, 'commits_by_author.dat' using 1:5 title "Yukihiro Matz Matsumoto" w lines, 'commits_by_author.dat' using 1:6 title "Masaki Muranaka" w lines, 'commits_by_author.dat' using 1:7 title "Masamitsu MURASE" w lines, 'commits_by_author.dat' using 1:8 title "skandhas" w lines, 'commits_by_author.dat' using 1:9 title "Yuichiro MASUI" w lines, 'commits_by_author.dat' using 1:10 title "mattn" w lines, 'commits_by_author.dat' using 1:11 title "Paolo Bosetti" w lines, 'commits_by_author.dat' using 1:12 title "Hidetaka Takano" w lines, 'commits_by_author.dat' using 1:13 title "Jon" w lines, 'commits_by_author.dat' using 1:14 title "Akira Yumiyama" w lines, 'commits_by_author.dat' using 1:15 title "Tomoyuki Sahara" w lines, 'commits_by_author.dat' using 1:16 title "Mitchell Blank Jr" w lines, 'commits_by_author.dat' using 1:17 title "Carson McDonald" w lines, 'commits_by_author.dat' using 1:18 title "Patrick Hogan" w lines, 'commits_by_author.dat' using 1:19 title "Kazuki Tsujimoto" w lines, 'commits_by_author.dat' using 1:20 title "Cremno" w lines, 'commits_by_author.dat' using 1:21 title "Narihiro Nakamura" w lines
